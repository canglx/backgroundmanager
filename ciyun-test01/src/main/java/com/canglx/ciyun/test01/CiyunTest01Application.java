package com.canglx.ciyun.test01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiyunTest01Application {

    public static void main(String[] args) {
        SpringApplication.run(CiyunTest01Application.class, args);
    }

}
