/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren;

import com.alibaba.fastjson.JSON;
import io.renren.modules.admin.dao.WarmContentDao;
import io.renren.modules.admin.entity.PhoneEntity;
import io.renren.modules.admin.entity.WarmContentEntity;
import io.renren.modules.admin.service.PhoneService;
import io.renren.modules.admin.service.WarmContentService;
import io.renren.service.DynamicDataSourceTestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 多数据源测试
 *
 * @author Mark sunlightcs@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DynamicDataSourceTest {
    @Autowired
    private DynamicDataSourceTestService dynamicDataSourceTestService;
    @Autowired
    private WarmContentDao warmContentDao;
    @Autowired
    private PhoneService phoneService;

    @Autowired
    private WarmContentService warmContentService;
    @Test
    public void test(){
//        List<String> strings = this.warmContentService.queryDesc();
//        strings.forEach(System.out::println);
    }


//    @Test
//    public void queryByDesc() {
//        WarmContentEntity neiyi = this.warmContentDao.queryByDesc("neiyi");
//        System.out.println("neiyi = " + neiyi.toString());
//        String content = neiyi.getContent();
//        String[] split = content.split("----");
//        for (int i = 0; i < split.length; i++) {
//            System.out.println("split = " + split[i]);
//        }
//        String jsonString = JSON.toJSONString(split);
//        System.out.println("jsonString = " + jsonString);
//    }

}
