package io.renren.modules.generator.controller;

import io.renren.modules.admin.entity.PhoneEntity;
import io.renren.modules.admin.service.PhoneService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
class PhoneControllerTest {
    @Autowired
    private PhoneService phoneService;

    @Test
    void list() {
        List<PhoneEntity> list = this.phoneService.list();
        list.forEach((item) ->
                System.out.println("item = " + item));
    }

    @Test
    void info() {
    }

    @Test
    void save() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}