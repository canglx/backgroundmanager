package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.ManagerContentEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 10:59:54
 */
public interface ManagerContentService extends IService<ManagerContentEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);

    List<String> queryDesc(Long userId);

    ManagerContentEntity queryByDesc(String desc, String s, Long userId);
}

