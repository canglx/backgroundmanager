package io.renren.modules.admin.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.PhoneWorkDao;
import io.renren.modules.admin.entity.PhoneWorkEntity;
import io.renren.modules.admin.service.PhoneWorkService;


@Service("phoneWorkService")
public class PhoneWorkServiceImpl extends ServiceImpl<PhoneWorkDao, PhoneWorkEntity> implements PhoneWorkService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PhoneWorkEntity> page = this.page(
                new Query<PhoneWorkEntity>().getPage(params),
                new QueryWrapper<PhoneWorkEntity>()
        );

        return new PageUtils(page);
    }

}