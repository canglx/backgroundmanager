package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 10:54:23
 */
@Data
@TableName("tb_manager_work")
public class ManagerWorkEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String OPTTYPE = "admin";

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 任务id
	 */
	private Long workId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 任务名
	 */
	private String workName;
	/**
	 * 手机数量
	 */
	private Integer phoneNum;
	/**
	 * 手机编号集合
	 */
	private String phoneIds;
	/**
	 * 手机别名
	 */
	@TableField(exist = false)
	private String alias;
	/**
	 * 直播地址
	 */
	private String liveAddr;
	/**
	 * 评论开始
	 */
	private Integer commentDelayStart;
	/**
	 * 评论结束
	 */
	private Integer commentDelayEnd;
	/**
	 * 发送开始
	 */
	private Integer commentSendStart;
	/**
	 * 发送结束
	 */
	private Integer commentSendEnd;
	/**
	 * 评论类型
	 */
	private String commentType;
	private String shieldType;
	/**
	 * 时长
	 */
	private Integer time;
	/**
	 * 是否@
	 */
	private Integer isAt;
	/**
	 * 间隔
	 */
	private String sendGap;
	/**
	 * 关键词
	 */
	private String keyWords;
	/**
	 * 已回复数量
	 */
	private Integer respNum;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
