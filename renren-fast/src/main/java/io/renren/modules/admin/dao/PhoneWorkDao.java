package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.PhoneWorkEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-01 16:06:19
 */
@Mapper
public interface PhoneWorkDao extends BaseMapper<PhoneWorkEntity> {
	
}
