package io.renren.modules.admin.service.impl;

import io.renren.modules.admin.controller.PhoneController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.WarmUpDao;
import io.renren.modules.admin.entity.WarmUpEntity;
import io.renren.modules.admin.service.WarmUpService;


@Service("warmUpService")
public class WarmUpServiceImpl extends ServiceImpl<WarmUpDao, WarmUpEntity> implements WarmUpService {

    @Autowired
    private WarmUpDao warmUpDao;

    @Autowired
    private PhoneController phoneController;

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long userId) {
        IPage<WarmUpEntity> page = this.page(
                new Query<WarmUpEntity>().getPage(params),
                new QueryWrapper<WarmUpEntity>().orderByDesc("create_time").eq("user_id",userId)
        );
        List<WarmUpEntity> warmUpEntities = page.getRecords();
        StringBuffer sb = new StringBuffer();
        warmUpEntities.forEach(warmUpEntity -> {
            String phoneIds = warmUpEntity.getPhoneIds();
            if (phoneIds != null) {
                String[] split = phoneIds.trim().split(",");
                for (String s : split) {
                    if (s != null && s != ""){
                        String aliasById = phoneController.getAliasById(s,userId);
                        if (aliasById != "") {
                            sb.append(aliasById+",");
                        }
                    }
                }
                if (StringUtils.equals(sb.toString(),"")) {
                    warmUpEntity.setAlias("");
                }else {
//                    System.out.println("sb.toString() = " + sb.toString());
                    String s = sb.deleteCharAt(sb.length() - 1).toString();
                    warmUpEntity.setAlias(s);
                }
            }else warmUpEntity.setAlias("");
            sb.delete(0, sb.length());
        });
        page.setRecords(warmUpEntities);
        return new PageUtils(page);
    }

    @Override
    public String queryNameByDeviceId(String deviceId) {
        List<String> names  = this.warmUpDao.selectName(deviceId);
        String name = "";
        if (names != null && names.size() >= 1) {
            name = names.get(0);// 最近创建的为准
        } else return null;
        String workName = name;
        if (workName == "") {
            return null;
        }
        return workName;
    }

}