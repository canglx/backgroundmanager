package io.renren.modules.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.AccountContentDao;
import io.renren.modules.admin.entity.AccountContentEntity;
import io.renren.modules.admin.service.AccountContentService;


@Service("accountContentService")
public class AccountContentServiceImpl extends ServiceImpl<AccountContentDao, AccountContentEntity> implements AccountContentService {

    @Autowired
    private AccountContentDao accountContentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long userId) {
        IPage<AccountContentEntity> page = this.page(
                new Query<AccountContentEntity>().getPage(params),
                new QueryWrapper<AccountContentEntity>().eq("user_id",userId)
        );
        List<AccountContentEntity> accountContentEntities = page.getRecords();
        accountContentEntities.forEach(accountContentEntity -> {
            String content = accountContentEntity.getContent();
            if (content.length() > 110) {
                String substring = content.substring(0, 100);
                accountContentEntity.setContent(substring);
            }
        });
        page.setRecords(accountContentEntities);

        return new PageUtils(page);
    }

    @Override
    public AccountContentEntity queryByDesc(String desc, String workId, Long userId) {
        String str = desc.substring(0, 2);
        AccountContentEntity accountContentEntity = this.accountContentDao.selectOne(new QueryWrapper<AccountContentEntity>().like("`desc`", str).eq("work_id",workId).eq("user_id",userId));

        return accountContentEntity;
    }

    @Override
    public List<String> queryDesc(Long userId) {
        List<AccountContentEntity> accountContentEntities = this.accountContentDao.selectList(new QueryWrapper<AccountContentEntity>().eq("work_id", 1003l).eq("user_id",userId));
        List<String> descs = new ArrayList<>();
        accountContentEntities.forEach(warmContentEntity -> {
            descs.add(warmContentEntity.getDesc());
        });
        return descs;
    }

}