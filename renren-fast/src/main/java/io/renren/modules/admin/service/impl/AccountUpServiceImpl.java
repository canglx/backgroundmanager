package io.renren.modules.admin.service.impl;

import io.renren.modules.admin.controller.PhoneController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.AccountUpDao;
import io.renren.modules.admin.entity.AccountUpEntity;
import io.renren.modules.admin.service.AccountUpService;


@Service("accountUpService")
public class AccountUpServiceImpl extends ServiceImpl<AccountUpDao, AccountUpEntity> implements AccountUpService {
    @Autowired
    private PhoneController phoneController;
    @Autowired
    private AccountUpDao accountUpDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long userId) {
        IPage<AccountUpEntity> page = this.page(
                new Query<AccountUpEntity>().getPage(params),
                new QueryWrapper<AccountUpEntity>().orderByDesc("create_time").eq("user_id",userId)
        );
        List<AccountUpEntity> accountUpEntities = page.getRecords();
        StringBuffer sb = new StringBuffer();
        accountUpEntities.forEach(accountUpEntity -> {
            String phoneIds = accountUpEntity.getPhoneIds();
            if (phoneIds != null) {
                String[] split = phoneIds.trim().split(",");
                for (String s : split) {
                    if (s != null && s != ""){
                        String aliasById = phoneController.getAliasById(s,userId);
                        if (aliasById != "") {
                            sb.append(aliasById+",");
                        }
                    }
                }
                if (StringUtils.equals(sb.toString(),"")) {
                    accountUpEntity.setAlias("");
                }else {
                    String s = sb.deleteCharAt(sb.length() - 1).toString();
                    accountUpEntity.setAlias(s);
                }
            }else accountUpEntity.setAlias("");
            sb.delete(0, sb.length());
        });
        page.setRecords(accountUpEntities);
        return new PageUtils(page);
    }

    @Override
    public String queryNameByDeviceId(String deviceId) {
        List<String> names  = this.accountUpDao.selectName(deviceId);
        String name = "";
        if (names != null && names.size() >= 1) {
            name = names.get(0);// 最近创建的为准
        } else return null;
        String workName = name;
        if (workName == "") {
            return null;
        }
        return workName;

    }

}