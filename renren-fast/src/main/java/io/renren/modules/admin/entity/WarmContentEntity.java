package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-03 13:35:25
 */
@Data
@TableName("tb_warm_content")
public class WarmContentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 任务id
	 */
	@TableField("work_id")
	private String workId;
	/**
	 * 描述
	 */
	@TableField("`desc`")
	private String desc;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 用户id
	 */
	private Long userId;

}
