package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:21:19
 */
@Data
@TableName("tb_account_content")
public class AccountContentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 描述
	 */
	@TableField("`desc`")
	private String desc;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 
	 */
	private Long workId;
	/**
	 * 任务id
	 */
	private Long userId;

}
