package io.renren.modules.admin.controller;

import java.util.Arrays;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.renren.modules.admin.entity.vo.TaskReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.admin.entity.WorkEntity;
import io.renren.modules.admin.service.WorkService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.client.RestTemplate;


/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-01 16:06:19
 */
@RestController
@RequestMapping("admin/work")
public class WorkController {
    @Autowired
    private WorkService workService;


    /**
     * 用于后台发送任务
     */
    @GetMapping("send")
    public TaskReqVo getTaskReqVo(@RequestBody TaskReqVo reqVo){
        return reqVo;
    }


    public static void main(String[] args) {
        MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
        TaskReqVo taskReqVo = new TaskReqVo();
        taskReqVo.setUrl("#在抖音，记录美好生活#【张艳艳（永）】正在直播，来和我一起支持TA吧。复制下方链接，打开【抖音】，直接观看直播！ https://v.douyin.com/Jxxkc8P/");
        taskReqVo.setCommentType("neiyi");
        taskReqVo.setCommentDelayStart(20);
        taskReqVo.setCommentDelayEnd(20);
        taskReqVo.setCommentSendStart(5);
        taskReqVo.setCommentSendEnd(10);
        taskReqVo.setAtRate(100);

        map.add("optType","admin");
        map.add("params",taskReqVo);
        for(Object obj : map.keySet()){
            Object value = map.get(obj);
            System.out.println("value = " + value.toString());
        }
        String string = JSON.toJSONString(map);
        System.out.println("string = " + string);


    }



    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = workService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		WorkEntity work = workService.getById(id);

        return R.ok().put("work", work);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WorkEntity work){
		workService.save(work);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WorkEntity work){
		workService.updateById(work);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		workService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
