package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.WarmUpEntity;

import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-09 16:09:57
 */
public interface WarmUpService extends IService<WarmUpEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);

    String queryNameByDeviceId(String deviceId);
}

