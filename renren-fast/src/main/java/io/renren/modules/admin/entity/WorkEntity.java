package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-01 16:06:19
 */
@Data
@TableName("tb_work")
public class WorkEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final long WARM_ID = 1001L;  // 暖场任务
	private static final long MANAGER_ID = 1002L; //  管理员任务
	private static final long FEED_ACC_ID = 1003L; // 养号任务




	/**
	 * id
	 */
	@TableId
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 任务名
	 */
	private String name;
	/**
	 * 有效执行数
	 */
	private Integer relCount;
	/**
	 * 总执行数
	 */
	private Integer count;

}
