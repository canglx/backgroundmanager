package io.renren.modules.admin.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.admin.entity.PhoneWorkEntity;
import io.renren.modules.admin.service.PhoneWorkService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-01 16:06:19
 */
@RestController
@RequestMapping("admin/phonework")
public class PhoneWorkController {
    @Autowired
    private PhoneWorkService phoneWorkService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = phoneWorkService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		PhoneWorkEntity phoneWork = phoneWorkService.getById(id);

        return R.ok().put("phoneWork", phoneWork);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody PhoneWorkEntity phoneWork){
		phoneWorkService.save(phoneWork);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody PhoneWorkEntity phoneWork){
		phoneWorkService.updateById(phoneWork);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		phoneWorkService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
