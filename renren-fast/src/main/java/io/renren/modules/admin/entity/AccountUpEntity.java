package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:53:19
 */
@Data
@TableName("tb_account_up")
public class AccountUpEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String OPTTYPE = "yanghao";

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 任务id
	 */
	private Long wordId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 任务名
	 */
	private String wordName;
	/**
	 * 设备id
	 */
	private String phoneIds;
	/**
	 * 手机数量
	 */
	@TableField(exist = false)
	private int phoneNum;

	/**
	 * 设备别名
	 */
	@TableField(exist = false)
	private String alias;
	/**
	 * 评论开始
	 */
	private Integer commentDelayStart;
	/**
	 * 评论结束
	 */
	private Integer commentDelayEnd;
//	/**
//	 * 发送开始
//	 */
//	private Integer commentSendStart;
//	/**
//	 * 发送结束
//	 */
//	private Integer commentSendEnd;
	/**
	 * 评论类型
	 */
	private String commentType;
	/**
	 * 任务时长
	 */
	private Integer alarmTimeDelay;
	/**
	 * 喜欢概率
	 */
	private Integer rateLike;
	/**
	 * 评论概率
	 */
	private Integer rateComment;
	/**
	 * 收藏概率
	 */
	private Integer keepPro;
	/**
	 * 转发概率
	 */
	private Integer forwardPro;
	/**
	 * 浏览主页概率
	 */
	private Integer watchMainPro;
	/**
	 * 是否关注(1是0非)
	 */
	private Integer isAtten;
	/**
	 * 关注概率
	 */
	private Integer attenPro;
	/**
	 * 是否智能评论（1是0非）
	 */
	private Integer isSmartComment;
	/**
	 * 评论关键词
	 */
	private String keyWords;
	/**
	 * 浏览作品时长{"start":1,"end":3}
	 */
	private String watchTime;
	/**
	 * 设备状态
	 */
	private Integer phoneState;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
