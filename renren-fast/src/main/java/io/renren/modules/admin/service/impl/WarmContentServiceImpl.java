package io.renren.modules.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.WarmContentDao;
import io.renren.modules.admin.entity.WarmContentEntity;
import io.renren.modules.admin.service.WarmContentService;


@Service("warmContentService")
public class WarmContentServiceImpl extends ServiceImpl<WarmContentDao, WarmContentEntity> implements WarmContentService {

    @Autowired
    private WarmContentDao warmContentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long userId) {
        IPage<WarmContentEntity> page = this.page(
                new Query<WarmContentEntity>().getPage(params),
                new QueryWrapper<WarmContentEntity>().eq("user_id", userId)
        );
        List<WarmContentEntity> warmContentEntities = page.getRecords();
        warmContentEntities.forEach(warmContentEntity -> {
            String content = warmContentEntity.getContent();
            if (content.length() > 110) {
                String substring = content.substring(0, 100);
                warmContentEntity.setContent(substring);
            }
        });
        page.setRecords(warmContentEntities);
        return new PageUtils(page);
    }

    @Override
    public WarmContentEntity queryByDesc(String desc, String workId, Long userId) {
//        WarmContentEntity warmContentEntity = this.warmContentDao.queryByDesc(desc,workId);
        String str = desc.substring(0, 2);
        WarmContentEntity warmContentEntity = this.warmContentDao.selectOne(new QueryWrapper<WarmContentEntity>().like("`desc`", str).eq("work_id",workId).eq("user_id", userId));

        return warmContentEntity;
    }

    @Override
    public List<String> queryDesc(Long userId) {
        List<WarmContentEntity> warmContentEntities = this.warmContentDao.selectList(new QueryWrapper<WarmContentEntity>().eq("work_id", 1001l).eq("user_id",userId));
        List<String> descs = new ArrayList<>();
        warmContentEntities.forEach(warmContentEntity -> {
            descs.add(warmContentEntity.getDesc());
        });
        return descs;
    }
}