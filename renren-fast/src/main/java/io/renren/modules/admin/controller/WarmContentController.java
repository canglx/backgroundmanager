package io.renren.modules.admin.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.utils.ShiroUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.admin.entity.WarmContentEntity;
import io.renren.modules.admin.service.WarmContentService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-03 13:35:25
 */
@RestController
@RequestMapping("admin/warmcontent")
public class WarmContentController {
    @Autowired
    private WarmContentService warmContentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登陆");
        }
        PageUtils page = warmContentService.queryPage(params,userId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		WarmContentEntity warmContent = warmContentService.getById(id);
        String content = warmContent.getContent();
        String replaceAll = content.replaceAll("\\|", "\\\n");
        warmContent.setContent(replaceAll);
        return R.ok().put("warmContent", warmContent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WarmContentEntity warmContent){
        String content = warmContent.getContent();
        String[] split = content.split("\\n");
        warmContent.setWorkId("1001");
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登陆");
        }
        warmContent.setUserId(userId);
        warmContentService.save(warmContent);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WarmContentEntity warmContent){
        String contentStr = warmContent.getContent();
        String[] split = contentStr.split("\\n");

        String join = StringUtils.join(split, "|");
        warmContent.setContent(join);
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登陆");
        }
        warmContent.setUserId(userId);
        warmContentService.updateById(warmContent);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		warmContentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
