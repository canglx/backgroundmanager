package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.ManagerContentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 10:59:54
 */
@Mapper
public interface ManagerContentDao extends BaseMapper<ManagerContentEntity> {

//    ManagerContentEntity queryByDesc(String desc, String workId);
}
