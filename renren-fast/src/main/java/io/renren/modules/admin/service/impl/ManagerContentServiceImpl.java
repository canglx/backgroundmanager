package io.renren.modules.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.ManagerContentDao;
import io.renren.modules.admin.entity.ManagerContentEntity;
import io.renren.modules.admin.service.ManagerContentService;


@Service("managerContentService")
public class ManagerContentServiceImpl extends ServiceImpl<ManagerContentDao, ManagerContentEntity> implements ManagerContentService {

    @Autowired
    private ManagerContentDao managerContentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long userId) {
        IPage<ManagerContentEntity> page = this.page(
                new Query<ManagerContentEntity>().getPage(params),
                new QueryWrapper<ManagerContentEntity>().eq("user_id",userId)
        );
        List<ManagerContentEntity> managerContentEntities = page.getRecords();
        managerContentEntities.forEach(managerContentEntity -> {
            String content = managerContentEntity.getContent();
            if (content.length() > 110) {
                String substring = content.substring(0, 100);
                managerContentEntity.setContent(substring);
            }
        });
        page.setRecords(managerContentEntities);

        return new PageUtils(page);
    }

    @Override
    public ManagerContentEntity queryByDesc(String desc, String workId, Long userId) {
//        ManagerContentEntity managerContentEntity = this.managerContentDao.queryByDesc(desc,workId);
//        ManagerContentEntity managerContentEntity = new ManagerContentEntity();
        String str = desc.substring(0, 2);
        ManagerContentEntity managerContentEntity = this.managerContentDao.selectOne(new QueryWrapper<ManagerContentEntity>().like("`desc`", str).eq("work_id",workId).eq("user_id",userId));

        return managerContentEntity;
    }

    @Override
    public List<String> queryDesc(Long userId) {
        List<ManagerContentEntity> managerContentEntities = this.managerContentDao.selectList(new QueryWrapper<ManagerContentEntity>().eq("work_id", 1002l).eq("user_id",userId));
        List<String> descs = new ArrayList<>();
        managerContentEntities.forEach(warmContentEntity -> {
            descs.add(warmContentEntity.getDesc());
        });
        return descs;
    }

}