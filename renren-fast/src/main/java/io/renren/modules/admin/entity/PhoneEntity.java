package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-01 16:06:19
 */
@Data
@TableName("tb_phone")
public class PhoneEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String imei;
	/**
	 * 
	 */
	private String imsi;
	/**
	 * 
	 */
	private String androidId;
	/**
	 * 
	 */
	private String loginIp;
	/**
	 * 
	 */
	private Date loginTime;

}
