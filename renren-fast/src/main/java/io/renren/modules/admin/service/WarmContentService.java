package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.WarmContentEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-03 13:35:25
 */
public interface WarmContentService extends IService<WarmContentEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);

    /**
     * 根据desc查询
     */
    WarmContentEntity queryByDesc(String desc, String workId, Long userId);

    List<String> queryDesc(Long userId);
}

