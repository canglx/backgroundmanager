package io.renren.modules.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-09 16:09:57
 */
@Data
@TableName("tb_warm_up")
public class WarmUpEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String OPTTYPE = "nuanchang";

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 任务id
	 */
	private Long wordId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 任务名
	 */
	private String wordName;
	/**
	 * 手机数量
	 */
	@TableField(exist = false)
	private Integer phoneNum;
	/**
	 * 手机编号
	 */
	@TableField(exist = false)
	private Long phoneId;
//	@TableField(exist = false)
	private String phoneIds;
	/**
	 * 设备名称
	 */
	@TableField(exist = false)
	private String alias;
	/**
	 * 直播地址
	 */
	private String liveAddr;
	/**
	 * 评论延迟先
	 */
	private Integer commentDelayStart;
	/**
	 * 评论延迟后
	 */
	private Integer commentDelayEnd;
	/**
	 * 发送延迟先
	 */
	private Integer commentSendStart;
	/**
	 * 发送延迟后
	 */
	private Integer commentSendEnd;
	/**
	 * 评论类型
	 */
	private String commentType;
	/**
	 * 是否点赞(1是0非)
	 */
	private Integer isLike;
	/**
	 * 是否点击小黄车(1是0非,默认不点)
	 */
	private Integer isCar;
	/**
	 * 发送顺序(0随机 1顺序, 默认随机)
	 */
	private Integer sendMod;
	/**
	 * 设备状态（0空闲，1工作中，-1异常）
	 */
	private Integer phoneState;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
