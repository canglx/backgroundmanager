package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.AccountUpEntity;

import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:53:19
 */
public interface AccountUpService extends IService<AccountUpEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);

    String queryNameByDeviceId(String deviceId);

}

