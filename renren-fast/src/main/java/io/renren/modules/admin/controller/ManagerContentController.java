package io.renren.modules.admin.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.utils.ShiroUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.admin.entity.ManagerContentEntity;
import io.renren.modules.admin.service.ManagerContentService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 10:59:54
 */
@RestController
@RequestMapping("admin/managercontent")
public class ManagerContentController {
    @Autowired
    private ManagerContentService managerContentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        }
        PageUtils page = managerContentService.queryPage(params,userId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		ManagerContentEntity managerContent = managerContentService.getById(id);
        String content = managerContent.getContent();
        String replaceAll = content.replaceAll("\\|", "\\\n");
        managerContent.setContent(replaceAll);
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        }
        managerContent.setUserId(userId);
        return R.ok().put("managerContent", managerContent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ManagerContentEntity managerContent){
        String content = managerContent.getContent();
        String[] split = content.split("\\n");
        String join = StringUtils.join(split, "|");
        managerContent.setContent(join);
        managerContent.setWorkId("1002");
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        };
        managerContent.setUserId(userId);
		managerContentService.save(managerContent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ManagerContentEntity managerContent){
        String contentStr = managerContent.getContent();
        String[] split = contentStr.split("\\n");

        String join = StringUtils.join(split, "|");
        managerContent.setContent(join);
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        }
        managerContent.setUserId(userId);
		managerContentService.updateById(managerContent);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		managerContentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
