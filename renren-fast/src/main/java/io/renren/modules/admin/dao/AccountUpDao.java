package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.AccountUpEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:53:19
 */
@Mapper
public interface AccountUpDao extends BaseMapper<AccountUpEntity> {

    List<String> selectName(String id);
}
