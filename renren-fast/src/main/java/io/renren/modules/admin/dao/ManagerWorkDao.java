package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.ManagerWorkEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 10:54:23
 */
@Mapper
public interface ManagerWorkDao extends BaseMapper<ManagerWorkEntity> {

    List<String> selectName(String id);
}
