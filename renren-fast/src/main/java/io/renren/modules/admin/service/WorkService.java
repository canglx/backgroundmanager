package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.WorkEntity;

import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-01 16:06:19
 */
public interface WorkService extends IService<WorkEntity> {

    PageUtils queryPage(Map<String, Object> params);




}

