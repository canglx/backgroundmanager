package io.renren.modules.admin.entity.vo;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.Date;

/**
 * @author fangzq
 */
/*
"task": {
					"data": "",
					"endTime": 0,
					"startTime": 0,
					"status": 0,
					"type": 0
				}
 */
@Data
public class TaskVo {
    private JSON data; //任务数据  json
    private String endTime; //任务开始时间
    private String startTime; // 任务结束时间

    private int status; // 任务状态
    private int type; //任务类型

}
