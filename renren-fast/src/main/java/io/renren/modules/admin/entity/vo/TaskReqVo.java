package io.renren.modules.admin.entity.vo;

import lombok.Data;

/**
 * 请求task任务的参数vo类
 */
/*
        "url":"#在抖音，记录美好生活#【张艳艳（永）】正在直播，来和我一起支持TA吧。复制下方链接，打开【抖音】，直接观看直播！ https://v.douyin.com/Jxxkc8P/",
        "commentType":"neiyi",
        "commentDelayStart":20,
        "commentDelayEnd":20,
        "commentSendStart":5,
        "commentSendEnd":10,
        "atRate":100
 */
@Data
public class TaskReqVo {
    private String url;  // 直播地址
    private String apiUrl; // 对应接口地址
    private String taskName;

    private String commentType;  // 目前对应评论内容里的desc
    private int commentDelayStart; //
    private int commentDelayEnd; //
    private int commentSendStart; // 评论间隔小
    private int commentSendEnd; // 评论间隔大
    private int atRate;  // 评论概率

}
