package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.ManagerWorkEntity;

import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 10:54:23
 */
public interface ManagerWorkService extends IService<ManagerWorkEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);

    String queryNameByDeviceId(String deviceId);
}

