package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.AccountContentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:21:19
 */
@Mapper
public interface AccountContentDao extends BaseMapper<AccountContentEntity> {
	
}
