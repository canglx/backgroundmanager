package io.renren.modules.admin.entity.vo;

import lombok.Data;

@Data
public class SendTaskVo {
    private String optType;
//    private String taskName;
    private Object params;

    public SendTaskVo(String optType,Object params){
        this.optType = optType;
        this.params = params;
    }
//    public SendTaskVo(String optType,String taskName,Object params){
//        this.optType = optType;
//        this.taskName = taskName;
//        this.params = params;
//    }
}
