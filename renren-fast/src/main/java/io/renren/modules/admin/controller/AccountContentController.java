package io.renren.modules.admin.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.admin.entity.AccountUpEntity;
import io.renren.modules.admin.entity.ManagerContentEntity;
import io.renren.modules.admin.entity.vo.AccountTaskReqVo;
import io.renren.modules.admin.entity.vo.DevHttpJsonVo;
import io.renren.modules.admin.entity.vo.ManagerTaskReqVo;
import io.renren.modules.admin.entity.vo.SendTaskVo;
import io.renren.modules.admin.service.AccountUpService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.admin.entity.AccountContentEntity;
import io.renren.modules.admin.service.AccountContentService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.client.RestTemplate;


/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:21:19
 */
@RestController
@RequestMapping("admin/accountcontent")
public class AccountContentController {
    @Autowired
    private AccountContentService accountContentService;


    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        }
        PageUtils page = accountContentService.queryPage(params,userId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        AccountContentEntity accountContent = accountContentService.getById(id);
        String content = accountContent.getContent();
        String replaceAll = content.replaceAll("\\|", "\\\n");
        accountContent.setContent(replaceAll);
        return R.ok().put("accountContent", accountContent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AccountContentEntity accountContent){
        String content = accountContent.getContent();
        String[] split = content.split("\\n");
        String join = StringUtils.join(split, "|");
        accountContent.setContent(join);
        accountContent.setWorkId(1003l);
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        }
        accountContent.setUserId(userId);
		accountContentService.save(accountContent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AccountContentEntity accountContent){
        String contentStr = accountContent.getContent();
        String[] split = contentStr.split("\\n");

        String join = StringUtils.join(split, "|");
        accountContent.setContent(join);
        Long userId = ShiroUtils.getUserId();
        if (userId == null) {
            return R.error("登录信息出错，请重新登录");
        }
        accountContent.setUserId(userId);
        accountContentService.updateById(accountContent);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		accountContentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
