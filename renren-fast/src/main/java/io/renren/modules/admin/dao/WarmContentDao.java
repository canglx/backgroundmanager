package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.WarmContentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.SysConfigEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-03 13:35:25
 */
@Mapper
public interface WarmContentDao extends BaseMapper<WarmContentEntity> {
    /**
     * 根据描述信息找到评论内容
     */
//    WarmContentEntity queryByDesc(String desc,String workId);
}
