package io.renren.modules.admin.entity.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

@Data
public class DevHttpJsonVo {
    private int code;
    private String  msg;
    private JSONObject data;
}
