package io.renren.modules.admin.entity.vo;

import lombok.Data;

/**
 * 请求task任务的参数vo类
 */

@Data
public class AccountTaskReqVo {
//    private String url;  // 养号任务不需要直播地址
//    private String apiPinBiUrl; // 屏蔽关键词url
    private String apiUrl; // 评论内容
    private String commentType;  // 目前对应评论内容里的desc
    private String taskName;
    private int commentDelayStart; //
    private int commentDelayEnd; //
    private int rateComment; // 评论概率
    private int rateLike; // 点喜欢概率
    private int alarmTimeDelay;  // 时长

}
