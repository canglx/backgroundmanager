package io.renren.modules.admin.entity.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.Date;

/**
 * @author fangzq
 */
@Data
public class PhoneEntityVo {
    private String deviceId;
    private String name;
    private String alias; // 设备别名
    // 任务名
    private String taskName;
    private String imei;
    private String imsi;
    private String androidId;
    private String sessionId;
    private String loginIp;
    private long loginTime;
    private Date loginDate;
    private int status;  // 0 正常   1 在线    2  离线
    private JSONObject task; // TODO 类型未知




//    private String alias;// 设备别名
//    private String androidId;
//    private long deviceId;  // 设备id
//    private String imei;
//    private String imsi;
//
//    private String loginIp;
//    private Date loginTime;
//
//    private String name;
//    private String sessionId; // 会话id
//    private int status;  // 设备状态
//
//    private TaskVo task; // 执行的任务

}
