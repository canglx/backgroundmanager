package io.renren.modules.admin.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.PhoneDao;
import io.renren.modules.admin.entity.PhoneEntity;
import io.renren.modules.admin.service.PhoneService;


@Service("phoneService")
public class PhoneServiceImpl extends ServiceImpl<PhoneDao, PhoneEntity> implements PhoneService {

    @Autowired
    private PhoneDao phoneDao;

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//        IPage<PhoneEntity> page = this.page(
//                new Query<PhoneEntity>().getPage(params),
//                new QueryWrapper<PhoneEntity>()
//        );
//
//        return new PageUtils(page);
//    }
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String imei = (String)params.get("imei");
        IPage<PhoneEntity> page = this.page(
                new Query<PhoneEntity>().getPage(params),
                new QueryWrapper<PhoneEntity>()
                        .eq(StringUtils.isNotBlank(imei), "imei", imei)
        );

        return new PageUtils(page);

    }

    @Override
    public String queryAliasById(String phoneId) {
        return null;
    }


}