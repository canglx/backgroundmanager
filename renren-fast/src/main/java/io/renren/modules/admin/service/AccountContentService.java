package io.renren.modules.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.admin.entity.AccountContentEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-11 19:21:19
 */
public interface AccountContentService extends IService<AccountContentEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);

    AccountContentEntity queryByDesc(String desc, String s, Long userId);

    List<String> queryDesc(Long userId);

}

