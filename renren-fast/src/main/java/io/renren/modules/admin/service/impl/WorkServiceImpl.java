package io.renren.modules.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.admin.dao.WorkDao;
import io.renren.modules.admin.entity.WorkEntity;
import io.renren.modules.admin.service.WorkService;


@Service("workService")
public class WorkServiceImpl extends ServiceImpl<WorkDao, WorkEntity> implements WorkService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WorkEntity> page = this.page(
                new Query<WorkEntity>().getPage(params),
                new QueryWrapper<WorkEntity>()
        );


        return new PageUtils(page);
    }

}