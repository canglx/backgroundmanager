package io.renren.modules.admin.dao;

import io.renren.modules.admin.entity.WarmUpEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author fangzq
 * @email canglx1996@163.com
 * @date 2020-12-09 16:09:57
 */
@Mapper
public interface WarmUpDao extends BaseMapper<WarmUpEntity> {

    List<String> selectName(String id);
}
